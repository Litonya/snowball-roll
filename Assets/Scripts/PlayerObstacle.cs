using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObstacle : MonoBehaviour
{
    protected int id;

    protected SoundManager soundManager;

    protected bool inPlacement = true;
    protected GameManager gameManager;
    protected Camera mainCamera;
    protected Collider selfCollider;
    protected float rotateSpeed = 35;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = gameManager.getMainCamera();
        selfCollider = GetComponent<Collider>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (inPlacement)
        {
            RaycastHit hit;
            LayerMask mask = LayerMask.GetMask("Terrain");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            {
                Debug.Log("hit");
                transform.position = hit.point;
            }
            else
            {
                Vector3 mousePosition = Input.mousePosition;
                transform.position = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, -mainCamera.transform.position.z + 6));
            }

            float scrollwheel = Input.GetAxis("Mouse ScrollWheel");
            if (scrollwheel!=0)
            {
                transform.Rotate(new Vector3(0, scrollwheel*rotateSpeed, 0), Space.Self);
            }
            if (!gameManager.getBuildModeState())
            {
                gameManager.objectSell(id, gameObject);
            }
        }
    }

    public void setGameManager(GameManager newGameManager)
    {
        gameManager = newGameManager;
    }

    protected void OnMouseDown()
    {
        if (gameManager.getBuildModeState())
        {
            if (inPlacement)
            {
                
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                LayerMask mask = LayerMask.GetMask("Terrain");
                if (!Physics.Raycast(ray, Mathf.Infinity, mask))
                {
                    soundManager.playSellObjectSound();
                    gameManager.objectSell(id, gameObject);
                }
                else
                {
                    soundManager.playPlaceObjectSound();
                    inPlacement = false;
                    selfCollider.isTrigger = false;
                }
            }
            else
            {
                soundManager.playGetObjectSound();
                inPlacement = true;
                selfCollider.isTrigger = true;
            }
        }
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setSoundManager(SoundManager soundManager)
    {
        this.soundManager = soundManager;
    }
}
