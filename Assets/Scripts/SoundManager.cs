using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private DataManager dataManager = new DataManager();

    [SerializeField]
    private AudioSource windSound;

    [SerializeField]
    private AudioSource buttonSound;

    [SerializeField]
    private AudioSource placeObjectSound;

    [SerializeField]
    private AudioSource getObjectSound;

    [SerializeField]
    private AudioSource sellObjectSound;

    [SerializeField]
    private AudioSource levelCompleteSound;

    [SerializeField]
    private AudioSource musicSource;

    [SerializeField]
    private AudioClip[] musicPlaylist;

    [SerializeField]
    private AudioSource boostSound;


    private void Start()
    {
        dataManager.Load();
        setSoundVolume(dataManager.GetSoundVolume());
        changeCurrentSong();
    }

    private void Update()
    {
        if (!musicSource.isPlaying) { changeCurrentSong(); }
    }

    public void playButtonSound()
    {
        buttonSound.Play();
    }

    public void playBoostSound()
    {
        boostSound.Play();
    }

    public void playPlaceObjectSound()
    {
        placeObjectSound.Play();
    }

    public void playGetObjectSound()
    {
        getObjectSound.Play();
    }

    public void playSellObjectSound()
    {
        sellObjectSound.Play();
    }

    public void playLevelCompleteSound()
    {
        levelCompleteSound.Play();
    }

    private void changeCurrentSong()
    {
        int i = Random.Range(0, musicPlaylist.Length);
        musicSource.clip = musicPlaylist[i];
        musicSource.Play();
    }

    public void setSoundVolume(float volume)
    {
        windSound.volume = volume;
        buttonSound.volume = volume;
        placeObjectSound.volume = volume;
        getObjectSound.volume = volume;
        sellObjectSound.volume = volume;
        levelCompleteSound.volume = volume;
        musicSource.volume = volume;
        boostSound.volume = volume;
    }
}
