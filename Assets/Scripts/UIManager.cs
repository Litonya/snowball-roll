using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private SoundManager soundManager;

    // Game Manager
    [SerializeField]
    private GameManager gameManager;

    // UI Text GameObjects
    /// Score
    public GameObject score_player;
    public GameObject goal_score;
    public GameObject best_score;
    /// Items Authorized
    public GameObject number_items_authorized;
    public GameObject max_items_authorized;
    /// Items features
    public GameObject price_item_1;
    public GameObject price_item_2;
    public GameObject price_item_3;
    public GameObject price_item_4;
    public GameObject price_item_5;
    public GameObject price_item_6;


    // Text Components
    /// Score
    private TextMeshProUGUI textmeshpro_player_score;
    private TextMeshProUGUI textmeshpro_goal_score;
    private TextMeshProUGUI textmeshpro_best_score;
    /// Items Authorized
    private TextMeshProUGUI textmeshpro_number_items_authorized;
    private TextMeshProUGUI textmeshpro_max_items_authorized;
    /// Items features
    private TextMeshProUGUI textmeshpro_price_item_1;
    private TextMeshProUGUI textmeshpro_price_item_2;
    private TextMeshProUGUI textmeshpro_price_item_3;
    private TextMeshProUGUI textmeshpro_price_item_4;
    private TextMeshProUGUI textmeshpro_price_item_5;
    private TextMeshProUGUI textmeshpro_price_item_6;

    //Level Accomplish Panel
    [SerializeField]
    private GameObject level_accomplish_panel;

    //Buttons
    [SerializeField]
    private Button start_button;
    [SerializeField]
    private Button reset_button;
    [SerializeField]
    private Button exit_button;

    // Start is called before the first frame update
    public void Awake()
    {
        // Text Mesh Pro
        /// Score
        textmeshpro_player_score = score_player.GetComponent<TextMeshProUGUI>();
        textmeshpro_goal_score = goal_score.GetComponent<TextMeshProUGUI>();
        textmeshpro_best_score = best_score.GetComponent<TextMeshProUGUI>();
        /// Items Authorized
        textmeshpro_number_items_authorized = number_items_authorized.GetComponent<TextMeshProUGUI>();
        textmeshpro_max_items_authorized = max_items_authorized.GetComponent<TextMeshProUGUI>();
        /// Items features
        textmeshpro_price_item_1 = price_item_1.GetComponent<TextMeshProUGUI>();
        textmeshpro_price_item_2 = price_item_2.GetComponent<TextMeshProUGUI>();
        textmeshpro_price_item_3 = price_item_3.GetComponent<TextMeshProUGUI>();
        textmeshpro_price_item_4 = price_item_4.GetComponent<TextMeshProUGUI>();
        textmeshpro_price_item_5 = price_item_5.GetComponent<TextMeshProUGUI>();
        textmeshpro_price_item_6 = price_item_6.GetComponent<TextMeshProUGUI>();
    }

    //
    public void InitItems() 
    {
        Debug.Log("Init Items");

        // init
        /// Items features
        textmeshpro_price_item_1.text = "1";
        textmeshpro_price_item_2.text = "2";
        textmeshpro_price_item_3.text = "3";
        textmeshpro_price_item_4.text = "2";
        textmeshpro_price_item_5.text = "1";
        textmeshpro_price_item_6.text = "2";
    }

    // 
    public void StartGame()
    {
        Debug.Log("Start game");
        soundManager.playButtonSound();
        gameManager.setGamePlay();
    }

    // 
    public void ResetGame() 
    {
        soundManager.playButtonSound();
        Debug.Log("Reset items");
        gameManager.reset();
    }

    // 
    public void ExitGame()
    {
        Debug.Log("Exit game");
        soundManager.playButtonSound();
        SceneManager.LoadScene("Menu");
    }

    //
    public void UpdatePointsAvailable(int points)
    {
        Debug.Log("Update of available points");
        textmeshpro_number_items_authorized.text = points.ToString();
    }

    //
    public void UpdateMaxPoints(int maxPoints)
    {
        Debug.Log("Update of max points");
        textmeshpro_max_items_authorized.text = maxPoints.ToString();        
    }

    //
    public void UpdatePlayerScore(float score)
    {
        Debug.Log("Update score player");
        textmeshpro_player_score.text = score.ToString("F0");
    }

    //
    public void UpdateGoalScore(float score)
    {
        Debug.Log("Update score goal");
        textmeshpro_goal_score.text = score.ToString();
    }

    //
    public void UpdateBestScore(float score)
    {
        Debug.Log("Update score best");
        textmeshpro_best_score.text = score.ToString("F0");
    }

    public void ObstacleBuy(int id)
    {
        Debug.Log("Ask creation of a new obstacle");
        soundManager.playButtonSound();
        gameManager.createNewObstacle(id);
    }

    public void NextLevel()
    {
        soundManager.playButtonSound();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ShowLevelAccomplishPanel()
    {
        level_accomplish_panel.SetActive(true);
        soundManager.playLevelCompleteSound();
        ChangeButtonsState(false);
    }

    public void ResetFromAccomplishPanel()
    {
        level_accomplish_panel.SetActive(false);
        soundManager.playButtonSound();
        ChangeButtonsState(true);
        ResetGame();
    }

    private void ChangeButtonsState(bool state)
    {
        start_button.enabled = state;
        exit_button.enabled = state;
        reset_button.enabled = state;
    }

    //
    public void UpdateCostObjects(int cost, int obj)
    {
        switch(obj+1)
        {
            case 1:
                Debug.Log("Update price item 1: "+cost);
                textmeshpro_price_item_1.text = cost.ToString();
                break;
            case 2:
                Debug.Log("Update price item 2: " + cost);
                textmeshpro_price_item_2.text = cost.ToString();
                break;
            case 3:
                Debug.Log("Update price item 3: " + cost);
                textmeshpro_price_item_3.text = cost.ToString();
                break;
            case 4:
                Debug.Log("Update price item 4: " + cost);
                textmeshpro_price_item_4.text = cost.ToString();
                break;
            case 5:
                Debug.Log("Update price item 5: " + cost);
                textmeshpro_price_item_5.text = cost.ToString();
                break;
            case 6:
                Debug.Log("Update price item 6: " + cost);
                textmeshpro_price_item_6.text = cost.ToString();
                break;
        }
    }
}
