using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private SoundManager soundManager;

    [SerializeField]
    Slider slider;

    private DataManager dataManager = new DataManager();

    private void Awake()
    {
        dataManager.Load();
    }

    private void Start()
    {
        slider.value = dataManager.GetSoundVolume();
    }

    public void PlayGame()
    {
        Debug.Log("Play");
        soundManager.playButtonSound();
        SceneManager.LoadScene("Level1");
    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        soundManager.playButtonSound();
        Application.Quit();
    }

    public void OptionApply()
    {
        soundManager.setSoundVolume(slider.value);
        soundManager.playButtonSound();
        dataManager.UpdateSoundVolume(slider.value);
        dataManager.Save();
    }

}
