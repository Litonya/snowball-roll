// 

using System.IO;
using UnityEngine;

public class DataManager
{
    //
    private GameData gameData;

    //
    private string settingsData = "settings.json";

    // 
    public void Load()
    {
        Debug.Log("Load Data");
        gameData = new GameData();
        string json = ReadFromFIle();
        gameData = JsonUtility.FromJson<GameData>(json);
    }

    //
    public void Save()
    {
        Debug.Log("Save Data");
        string json = JsonUtility.ToJson(gameData);
        WriteToFile(json);
    }

    //
    private void WriteToFile(string json)
    {
        File.WriteAllText(GetFilePath(settingsData), json);
    }

    //
    private string ReadFromFIle()
    {
        string json = File.ReadAllText(GetFilePath(settingsData));
        Debug.Log("JSON: " + json);
        return json;
    }

    //
    private string GetFilePath(string fileData)
    {
        return Application.dataPath + "/" + fileData;
    }

    //
    public void UpdateBestScore(int level, float score)
    {
        Debug.Log("Update Best Score");
        gameData.listLevelData[level - 1].bestScore = score.ToString();
    }

    public void UpdateSoundVolume(float volume)
    {
        Debug.Log("Update sound volume");
        gameData.volume = volume;
    }

    //
    public string GetBestScore(int level)
    {
        Debug.Log("Get Best Score: " + gameData.listLevelData[level - 1].bestScore);
        return gameData.listLevelData[level - 1].bestScore;
    }

    public float GetSoundVolume()
    {
        //Debug.Log("Get Volume: " + gameData.volume);
        return gameData.volume;
    }

    //
    public string GetGoalScore(int level)
    {
        Debug.Log("Get Goal Score: "+ gameData.listLevelData[level - 1].goalScore);
        return gameData.listLevelData[level-1].goalScore;
    }

    //
    public string GetPointsAvailable(int level)
    {
        Debug.Log("Get Best Score: "+ gameData.listLevelData[level - 1].pointsAvailable);
        return gameData.listLevelData[level-1].pointsAvailable;
    }

    //
    public string GetPathObject(int level, int obj)
    {
        Debug.Log("Get Path Object: " + gameData.listLevelData[level - 1].listObjectsData[obj].pathObject);
        return gameData.listLevelData[level - 1].listObjectsData[obj].pathObject;
    }

    //
    public int GetCostObjects(int level, int obj)
    {
        Debug.Log("Get Cost Object: " + gameData.listLevelData[level - 1].listObjectsData[obj].cost);
        return gameData.listLevelData[level - 1].listObjectsData[obj].cost;
    }

    //
    public int GetNumberObjects(int level)
    {

        Debug.Log("Get Number Object: " + gameData.listLevelData[level - 1].listObjectsData.Count);
        return gameData.listLevelData[level - 1].listObjectsData.Count;
    }
}
