// 

using System.Collections.Generic;

[System.Serializable]
public class LevelData
{
    public int level;
    public string goalScore;
    public string bestScore;
    public string pointsAvailable;
    public List<ObjectData> listObjectsData = new List<ObjectData>();
}
