using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBoosObstacle : PlayerObstacle
{
    private bool boostAvailable = true;
    private float boostforce = 0.025f;

    [SerializeField]
    private GameObject arrow;

    [SerializeField]
    private Material activateMaterial;
    [SerializeField]
    private Material deasactivateMaterial;

    private void Update()
    {
        if (inPlacement)
        {
            RaycastHit hit;
            LayerMask mask = LayerMask.GetMask("Terrain");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            {
                Debug.Log("hit");
                transform.position = hit.point;
            }
            else
            {
                Vector3 mousePosition = Input.mousePosition;
                transform.position = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, -mainCamera.transform.position.z + 6));
            }

            float scrollwheel = Input.GetAxis("Mouse ScrollWheel");
            if (scrollwheel != 0)
            {
                transform.Rotate(new Vector3(0, scrollwheel * rotateSpeed, 0), Space.Self);
            }
            if (!gameManager.getBuildModeState())
            {
                gameManager.objectSell(id, gameObject);
            }
        }
        if (gameManager.getBuildModeState())
        {
            activateBoost();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && boostAvailable)
        {
            Debug.Log("BOOST!");
            soundManager.playBoostSound();
            Rigidbody playerRigidbody = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 forceVector = transform.TransformVector(Vector3.right);
            forceVector *= boostforce;
            playerRigidbody.AddForce(forceVector, ForceMode.Impulse);
            desactivateBoost();
        }
    }

    private void desactivateBoost()
    {
        boostAvailable = false;
        arrow.GetComponent<Renderer>().material = deasactivateMaterial;
    }

    private void activateBoost()
    {
        boostAvailable = true;
        arrow.GetComponent<Renderer>().material = activateMaterial;
    }
}
