using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    private GameManager gameManager;

    private Vector3 previousPos;

    [SerializeField]
    private float scaleMultiplier;
    private float defaultSize;

    public void setGameManager(GameManager newGameManager)
    {
        gameManager = newGameManager;
    }

    private void Awake()
    {
        updatePreviousPos();
        defaultSize = transform.localScale.x;
    }

    public void Update()
    {
        if (transform.position != previousPos)
        {
            float distanceMove = Vector3.Distance(previousPos, transform.position);
            float scaleChange = distanceMove * scaleMultiplier * Time.deltaTime;

            Vector3 vectorChange = new Vector3(scaleChange, scaleChange, scaleChange);
            transform.localScale += vectorChange;
        }

        updatePreviousPos();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EndLine")
        {
            gameManager.snowballEnd();
        }
    }

    private void updatePreviousPos()
    {
        previousPos = transform.position;
    }

    public void resetSize()
    {
        transform.localScale = new Vector3(defaultSize, defaultSize, defaultSize);
    }
    public void setPreviousPos(Vector3 postion)
    {
        previousPos = postion;
    }
}
