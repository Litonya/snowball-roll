// 

using System.Collections.Generic;

[System.Serializable]
public class GameData
{
    public string version;
    public float volume;
    public List<LevelData> listLevelData = new List<LevelData>();
}
