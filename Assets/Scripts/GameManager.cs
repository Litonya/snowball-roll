using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private SoundManager soundManager;

    private float scoreMultiplier = 500;


    [SerializeField]
    private GameObject snowball;
    private Rigidbody snowballRigidbody;
    private Snowball snowballScript;
    [SerializeField]
    private float score = 0;
    private float maxScore = 0;
    [SerializeField]
    private float scoreToReach;

    [SerializeField]
    private int objectPointMax;
    private int objectPoint;


    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private UIManager uiManager;
    private DataManager dataManager = new DataManager();

    [SerializeField]
    private bool gamePause = true;

    private Vector3 originalSnowBallPosition;
    private Quaternion originalSnowBallRotation;

    [SerializeField]
    private GameObject[] playerObstaclePrefabs;
    [SerializeField]
    private int[] playerObstacleCost;

    private float pistInclination = -50;

    [SerializeField]
    private bool isBuildMod = true;

    private float previousSize = 0;

    // Start is called before the first frame update
    void Awake()
    {
        //Assign fields
        snowballRigidbody = snowball.GetComponent<Rigidbody>();
        snowballScript = snowball.GetComponent<Snowball>();

        snowballScript.setGameManager(this);

        // Load settings
        dataManager.Load();
    }

    private void Start()
    {
        updateOriginalSnowball();
        InitLevel();
    }

    // Update is called once per frame
    void Update()
    {

        if (!gamePause)
        {
            //if (snowballRigidbody.velocity == Vector3.zero) { snowballStop(); }
            score += (snowball.transform.localScale.x - previousSize) * scoreMultiplier * Time.deltaTime;
            previousSize = snowball.transform.localScale.x;
            uiManager.UpdatePlayerScore(score);
        }
    }

    private void updateOriginalSnowball()
    {
        originalSnowBallPosition = snowball.transform.position;
        originalSnowBallRotation = snowball.transform.rotation;
    }



    //Use when the ball can't move anymore
    private void snowballStop()
    {
        score = 0;
        setGamePause(false);
        Debug.Log("Snowball stop, gamePause = " + gamePause.ToString());
    }

    public void snowballEnd()
    {
        setGamePause(false);
        if (score > maxScore) { maxScore = score; }
        uiManager.UpdateBestScore(maxScore);
        dataManager.UpdateBestScore(SceneManager.GetActiveScene().buildIndex, maxScore);
        dataManager.Save();
        Debug.Log("Snowball reach end, gamPause = " + gamePause.ToString());
        if (score >= scoreToReach) { levelAccomplish(); }
    }

    private void setGamePause(bool freeze)
    {
        gamePause = true;
        if (freeze) { snowballRigidbody.isKinematic = true; }
        Debug.Log("Game set on Pause mode, freez: " + freeze.ToString());
        
    }

    public void setGamePlay()
    {
        if (gamePause)
        {
            previousSize = snowball.transform.localScale.x;
            gamePause = false;
            isBuildMod = false;
            snowballRigidbody.isKinematic = false;
            Debug.Log("Game set on Play mode");
        }
    }

    private void levelAccomplish()
    {
        Debug.Log("Level accomplish with: " + maxScore.ToString());
        uiManager.ShowLevelAccomplishPanel();

    }

    public bool isPause() { return gamePause; }

    public void reset()
    {
        Debug.Log("Game Reset");
        isBuildMod = true;
        setGamePause(true);
        snowballScript.setPreviousPos(originalSnowBallPosition);
        snowball.transform.position = originalSnowBallPosition;
        snowball.transform.rotation = originalSnowBallRotation;
        snowballScript.resetSize();
        score = 0;
        uiManager.UpdatePlayerScore(score);
    }

    public void createNewObstacle(int id)
    {
        if (playerObstacleCost[id] <= objectPoint && isBuildMod)
        {
            Debug.Log("Obstacle buy for: " + playerObstacleCost[id].ToString());
            objectPoint -= playerObstacleCost[id];
            uiManager.UpdatePointsAvailable(objectPoint);
            Debug.Log(objectPoint.ToString() + " points left");

            Vector3 mousePosition = Input.mousePosition;
            Vector3 obstaclePlacement = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 0));

            Quaternion orientation = Quaternion.identity;
            orientation.eulerAngles = new Vector3(pistInclination, 0, 0);

            GameObject obstacle = Instantiate(playerObstaclePrefabs[id], obstaclePlacement, orientation);
            PlayerObstacle obstacleScript = obstacle.GetComponent<PlayerObstacle>();
            obstacleScript.setGameManager(this);
            obstacleScript.setId(id);
            obstacleScript.setSoundManager(soundManager);
            
        }
    }

    public void objectSell(int id, GameObject obstacle)
    {
        objectPoint += playerObstacleCost[id];
        uiManager.UpdatePointsAvailable(objectPoint);
        Destroy(obstacle);
        Debug.Log("Obstacle sell for: " + playerObstacleCost[id].ToString());
    }
    
    public Camera getMainCamera() { return mainCamera; }

    public bool getGameState() { return gamePause; }

    public bool getBuildModeState() { return isBuildMod; }

    //
    public void InitLevel()
    {
        int level = SceneManager.GetActiveScene().buildIndex;
        Debug.Log("level");

        objectPoint = objectPointMax;

        uiManager.UpdateGoalScore(scoreToReach);
        uiManager.UpdateBestScore(float.Parse(dataManager.GetBestScore(level)));
        uiManager.UpdatePointsAvailable(objectPoint);
        uiManager.UpdateMaxPoints(objectPointMax);

        int nbObj = playerObstacleCost.Length;
        for (int i=0; i < nbObj; i++)
        {
            Debug.Log("Object " + i);
            uiManager.UpdateCostObjects(playerObstacleCost[i], i);
        }
    }
}
